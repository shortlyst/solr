#!/bin/bash
echo "Current version numbers:"
git tag
git checkout develop; git pull
echo -ne '\n' | git flow init -f
echo -ne '\n'
echo "Enter the release version number"
read versionNumber
versionLabel=v$versionNumber
git flow release start $versionLabel
git flow release finish $versionLabel
git push; git push origin master; git push --tags; git push origin develop
